console.log("Hello World!");




// 3 and 4
function cube(cubeNum) {
	let cubeThis = cubeNum ** 3;
	console.log(`The cube of ${cubeNum} is ${cubeThis}.`);

};

cube(2);

// 5
let fullAddress = ["258", "Washington Ave", "NW", "California", "90011", "USA"];


// 6
const [houseNumber, street, city, state, zipCode, country] = fullAddress;

console.log(`I live at ${houseNumber} ${street} ${city}, ${state} ${zipCode}, ${country}.`)


//7
let animal = {
	name: "Cattle",
	speed: "40 km/h",
	dailySleep: "4 hours",
	scientificName: "Bos Taurus",

};

//8
const {name, speed, dailySleep, scientificName} = animal;

function getAnimalDetails ({name, speed, dailySleep, scientificName}) {
	console.log(`A ${name}'s scientific name is ${scientificName}, with an approximate speed of ${speed} and sleeps everyday for ${dailySleep}.`)
};

getAnimalDetails(animal);

//9 
const numbers = [5, 10, 15, 20, 25];

//10

numbers.forEach((number) => {
	console.log(number);
});

//11
class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

const myDog = new dog("Lablab", "7", "Labrador Retriever");
console.log(myDog);